#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2016 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import os
import sys
import getopt
import codecs
import re
import itertools
import csv

os.environ["DJANGO_SETTINGS_MODULE"] = "data2u.settings"
sys.path.append(os.path.expanduser(os.path.join('~', 'django')))

# Loads the models so that they are used within the script.
import django
django.setup()

print('File to evaluate the rules and insert something in the suggestion')
print('table')

