#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#

#
# The functions in this file are "Incremental Event Processing Functions".
# They are conceived to process a set of events that occurred from a
# date/time previously recorded. They are incrementally calculating a set of
# indicators that accumulate their values over time.
#
from __future__ import print_function

import json
import re
import sys
import time

from django.utils import timezone
from future.moves.urllib.parse import urlparse, urlencode, urlunparse, parse_qs

import accumulate.models
import leco.models
import user_mgmt.user_operations

# These should be the right imports for Python 3
# import urllib.parse
# import urllib.request, urllib.parse, urllib.error

# Dictionary with the empty payloads for the different indicator types
initial_payloads = {
    # Event indicators

    'CMT_YOURSTRATEGY': {'text': []},       # List of strings
    'EVC_COL_EXP': [0, 0],                  # Num collapse, Num expand
    'EVC_DBOARDVIEW': 0,                    # Simple counter, number of views
    'EVC_DOWNLOAD': 0,                      # Simple counter
    'EVC_DURATION': [0, 0, 0, 0, 0, 0, 0],  # Times each value in likert
    'EVC_EXCO': [0, {}],                    # Max score, {(ex, [corr, inc])}
    'EVC_FORM_SUBMIT': 0,                   # Counter
    'EVC_MCQ': [0, 0, 0],                   # [corr, incorr, show]
    'EVC_PIAZZA': {},                       # {days online, views, etc.}
    'EVC_RESOURCE_VIEW': 0,                 # Counter
    'EVC_VIDEO': [0, 0, 0, 0, 0, 0],        # [Play, Pause, loaded, End, secs,
                                            # timestamp]
    'EV_XY_MAP': [{}, {}, {}, {}],          # List with four maps. One per
                                            # quadrant. Maps  activity ID to
                                            # (title, url),
                                            # URL and quadrant (0-3) counter
                                            # Clockwise
    'SCORE': 0,                             # Store a numeric score
}


def spq_encode(answers):
    """Function that receives an array of 20 answers and encodes the results
       for the SPQ survey. It returns the following tuple:

       (DA: Deep approach,
        SA: Surface approach,
        DM: Deep motive,
        SM: Surface motive,
        DS: Deep strategy,
        SS: Surface strategy)

        They are the averages of the corresponding questions.
        :param answers: Array of 20 answers that encodes the results of the
        survey
    """

    dm_idx = [1, 5, 9, 13, 17]
    ds_idx = [2, 6, 10, 14, 18]
    sm_idx = [3, 7, 11, 15, 19]
    ss_idx = [4, 8, 12, 16, 20]

    # Calculate the four accumulations first
    dm_val = 1.0 * sum([answers[i - 1] for i in dm_idx])
    ds_val = 1.0 * sum([answers[i - 1] for i in ds_idx])
    sm_val = 1.0 * sum([answers[i - 1] for i in sm_idx])
    ss_val = 1.0 * sum([answers[i - 1] for i in ss_idx])

    # Return the six values
    return ((dm_val + ds_val) / (len(dm_idx) + len(ds_idx)),
            (sm_val + ss_val) / (len(sm_idx) + len(ss_idx)),
            dm_val / len(dm_idx),
            sm_val / len(sm_idx),
            ds_val / len(ds_idx),
            ss_val / len(ss_idx))


def get_or_create_event_counter(user, indicator_type, ctx_object, resource_id,
                                version):
    """
    Function that either gets or creates the item with the indicator with the
    given parameters.

    :param user
    :param indicator_type
    :param ctx_object
    :param resource_id
    :param version
    """

    global initial_payloads

    # context object should not be None
    assert ctx_object is not None, 'Empty context object'

    (indicator, created) = accumulate.models.Indicator.objects.get_or_create(
            user=user,
            type=indicator_type,
            context=ctx_object,
            resource_id=resource_id,
            version=version
    )

    if not created:
        # Obtained an indicator that already existed. Return
        return indicator, created

    # The indicator has been created new. Initialise the payload field
    indicator.payload = json.dumps(initial_payloads[indicator_type])
    indicator.save()
    return indicator, created


def get_indicator(user, indicator_type, ctx_object, resource_id, version):
    """
    Given:
    :param user  user_id
    :param indicator_type  string identifying the type of event
    :param ctx_object Object with the context information
    :param resource_id string identifying the activity for the indicator
    :param version version number for the indicator

    :return One row in the database including the data for the user.
    """

    # Get the data structure for the accumulation of events for the user
    user_obj, _ = get_or_create_event_counter(user, indicator_type,
                                              ctx_object,
                                              resource_id, version)

    return user_obj


def update_event_counters(ctx_object, ctx_handler, last_execution, options):
    """
    :param ctx_object: Context object in the database
    :param ctx_handler: Python module to handle this context
    :param last_execution: Date/time of the last execution
    :param options: Pairs name=value to pass to the filter
    :return:
    """

    # Used to measure the throughput (events/sec)
    start_time = time.time()

    # Get the value of maximum events from the filter options
    maximum_events = options.get('maximum_events', -1)

    action_filter = options.get('action')
    if action_filter is None:
        # The main query: get all the events in the context after the last
        # executed time mark
        if maximum_events == -1:
            events_to_process = \
                leco.models.Submission.objects.filter(
                    context=ctx_object.name,
                    id__gt=last_execution.id_last_event_processed)
        else:
            try:
                maximum_events = int(maximum_events)
            except ValueError:
                print('Incorrect integer value in option maximum_events.',
                      file=sys.stderr)
                return

            events_to_process = \
                leco.models.Submission.objects.filter(
                    context=ctx_object.name,
                    id__gt=last_execution.id_last_event_processed
                )[:maximum_events]
    else:
        # An action filter is given, forget about the time limit for the time
        # being.
        events_to_process = \
            leco.models.Submission.objects.filter(
                context=ctx_object.name,
                action_id=action_filter)

    # Main LOOP for each event to be processed!
    event_count = 0
    last_event_id = -1
    for event in events_to_process:

        # Remember last event id
        last_event_id = event.id

        # Count events
        event_count += 1

        if event.id > last_execution.id_last_event_processed:
            last_execution.id_last_event_processed = event.id

        # Obtain the version of events to process
        version = ctx_handler.get_version(event)

        # Process the event and update the indicators
        update_event_counter(ctx_object, event, version)

    # If the maximum of events has been passed, terminate.
    if (maximum_events != -1) and event_count != 0:
        print(file=sys.stderr)
        print('###', file=sys.stderr)
        print('### Only', maximum_events, 'processed. Stopping filter.',
              file=sys.stderr)
        print('###', file=sys.stderr)
        print(file=sys.stderr)

    # Notify the final event that has been processed.
    print('Final event id:', end=' ', file=sys.stderr)
    print(last_execution.id_last_event_processed, file=sys.stderr)
    if len(events_to_process) != 0:
        print('Events processed:', len(events_to_process), file=sys.stderr)
        print('Event ids:', events_to_process[0].id, 'to', last_event_id,
              file=sys.stderr)
    # Elapsed time
    elapsed_time = time.time() - start_time
    print('Speed:', end=' ', file=sys.stderr)
    print(1.0 * len(events_to_process) / elapsed_time, end=' ', file=sys.stderr)
    print('events/sec', file=sys.stderr)

    # Refresh the values in the Global variables
    last_execution.datetime_last_processed = timezone.now()
    last_execution.save()


def update_event_counter(ctx_object, event, version):
    """
    Receives event and version, and updates the appropriate event counter.
    This is a switchboard type of function. A common entry point
    that invokes different functions depending on the type of event received.

    :param ctx_object: Object in the DB with the context information
    :param event: DB row with the entire event
    :param version: integer stating the version of the event
    :return: Nothing, effect reflected in the Indicator table
    """

    # Sanity check to see if something went wrong and we are getting events
    # with a string encoding an empty payload.
    if event.payload == '{}':
        raise Exception('Empty payload when processing event' + str(event.id))

    user = event.user
    payload = json.loads(event.payload)

    if event.action_id == 'dboard-view':
        # DBOARDVIEW
        update_dboardview(version, user, ctx_object, payload)

    elif event.action_id == 'resource-view' and '_download' in event.payload:
        # DOWNLOAD
        print('DOWNLOAD not implemented', file=sys.stderr)

    elif event.action_id == 'activity-duration':
        # DURATION
        update_duration(version, user, ctx_object, payload)

    elif event.action_id == 'activity-collapse-expand':
        # COL_EXP
        update_collapse_expand(version, user, ctx_object, payload)

    elif event.action_id == 'exco-answer':
        # EXCO
        update_exco(version, user, ctx_object, payload)

    elif event.action_id == 'embedded-question':
        # MCQ
        update_mcq(version, user, ctx_object, payload)

    elif event.action_id == 'resource-view':
        # RESOURCE_VIEW
        update_resource_view(version, user, ctx_object, payload)

    elif event.action_id == 'embedded-video':
        # VIDEO
        update_video(version, user, ctx_object, payload)

    elif event.action_id == 'form-submit':
        # FORM_SUBMIT
        update_form_submit(version, user, ctx_object, payload)

    elif event.action_id == 'xy-click':
        # XY Click
        update_xyclick(user, ctx_object, payload)

    else:
        print('Event type ', event.action_id, end=' ', file=sys.stderr)
        print('not processed.', file=sys.stderr)


def update_dboardview(version, user, ctx_object, payload):
    """
    Process the new event of type dboardview and update the indicators.

    The data expected in the payload is:

    :param version:
    :param user: user to update
    :param ctx_object: context
    :param payload: Event payload such as, for example:

           {"week": 1,
            "values": [100, 55, 77, 67, 0, 17, 0, 85],
            "user": "<email of the user data shown>"}

    :return: Update the content in the database. More precisely, increase the
    number in the indicator for the given week (as activity_id)
    """

    activity_id = payload.get('week', None)
    if activity_id is None:
        raise Exception('DASHBOARD_VIEW event with incorrect payload: ' +
                        json.dumps(payload))

    user_i = get_indicator(user, 'EVC_DBOARDVIEW', ctx_object, activity_id,
                           version)

    # Update the user counter
    counter = json.loads(user_i.payload)
    counter += 1

    user_i.payload = json.dumps(counter)
    user_i.updated = timezone.now()
    user_i.save()


def update_duration(version, user, ctx_object, payload):
    """

    :param version: Version of the event
    :param user: user to update
    :param ctx_object: context
    :param payload: Event payload such as, for example

           {"activity-id": "a-word-about-your-strategy", "value": "30/30"}

    :return: Update the element in the DB. The payload for the indicator
    contains seven counters (one for each possible value fo the duration
    response)
    """

    activity_id = payload.get('activity-id', None)
    value = payload.get('value', None)
    if activity_id is None:
        print('DURATION event with incorrect format '
              + json.dumps(payload), file=sys.stderr)
        return

    if value is None:
        # No value submitted as duration, ignore the event
        return

    user_i = get_indicator(user, 'EVC_DURATION', ctx_object, activity_id,
                           version)

    # Process the value in the event
    center_value = int(value[value.rindex('/') + 1:])

    user_counts = json.loads(user_i.payload)

    # Detect all 7 possible values of the likert scale
    if value.startswith('LT-'):
        user_counts[0] += 1
    elif value.startswith('GT-'):
        user_counts[6] += 1
    else:
        # At this point we have a num/num value
        real_value = int(value[:value.rindex('/')])
        if real_value == center_value:
            user_counts[3] += 1
        elif real_value == (center_value / 2):
            user_counts[1] += 1
        elif real_value == (center_value * 3 / 4):
            user_counts[2] += 1
        elif real_value == (center_value * 5 / 4):
            user_counts[4] += 1
        elif real_value == (center_value * 3 / 2):
            user_counts[5] += 1
        else:
            raise Exception('ERROR: Unable to classify value' +
                            str(real_value))

    # Update in DB
    user_i.payload = json.dumps(user_counts)
    user_i.updated = timezone.now()
    user_i.save()


def update_collapse_expand(version, user, ctx_object, payload):
    """

    :param version:
    :param user: user to update
    :param ctx_object: context
    :param payload: Event payload such as, for example

           {"section": "lecture-week-01-lec",
           "page": "http://..../file.html",
           "operation": "expand"}

    :return: Update the indicators in the DB with the following structure:

             [#number of expand, #number of collapse]
    """

    url_str = payload.get('page', None)
    activity_id = payload.get('section', None)
    operation = payload.get('operation', None)
    if url_str is None or activity_id is None or operation is None:
        raise Exception('COL_EXP event with incorrect format ' +
                        json.dumps(payload))

    url_path = urlparse(url_str)
    url_key = urlunparse(('', '', url_path[2], url_path[3], url_path[4],
                          url_path[5]))
    # Remove the suffix index.html to make sure URLs are the same when they
    # have no file name
    url_key = re.sub('index\.html$', '', url_key)

    # Get section name and combine with page URL to uniquely identify event
    activity_id = url_key + '#' + activity_id

    user_i = get_indicator(user, 'EVC_COL_EXP', ctx_object, activity_id,
                           version)

    user_counters = json.loads(user_i.payload)

    if operation == 'expand':
        user_counters[0] += 1
    else:
        user_counters[1] += 1

    # Update information in DB
    user_i.payload = json.dumps(user_counters)
    user_i.updated = timezone.now()
    user_i.save()


def update_exco(version, user, ctx_object, payload):
    """

    :param version:
    :param user: user to update
    :param ctx_object: context
    :param payload: Event payload such as, for example

          {"outcome": "correct",
           "score": 100.0,
           "exercise":"file_path",
           "sequence": "COD-exco-C"}

    :return: Update the indicators in the DB. More precisely update the
    structure:

        [maximum score, {exercise: [#correct, #incorrect], ... }]
    """

    # Obtain the fields out of the event
    activity_id = payload.get('sequence', None)
    outcome = payload.get('outcome', None)
    exercise = payload.get('exercise', None)
    score = payload.get('score', None)

    # Check that the event is properly formed
    if activity_id is None or outcome is None or exercise is None or \
            score is None:
        raise Exception('EXCO event with incorrect format ' +
                        json.dumps(payload))

    user_i = get_indicator(user, 'EVC_EXCO', ctx_object, activity_id, version)

    # Load the JSON data for both indicators
    user_counter = json.loads(user_i.payload)

    # Update the score
    if score > user_counter[0]:
        user_counter[0] = score

    # Update the exercise counts
    exercise_counts = user_counter[1].get(exercise, None)
    if exercise_counts is None:
        exercise_counts = [0, 0]
        user_counter[1][exercise] = exercise_counts

    # Increment either counter
    if outcome == 'correct':
        exercise_counts[0] += 1
    else:
        exercise_counts[1] += 1

    # Update information in DB
    user_i.payload = json.dumps(user_counter)
    user_i.updated = timezone.now()
    user_i.save()


def update_mcq(version, user, ctx_object, payload):
    """

    :param version:
    :param user: user to update
    :param ctx_object: context
    :param payload: Event payload such as, for example

     {"answer": 1, "question_id": "COD-baseencoding-section-eqt_3"}

    :return: Update the indicators in the DB. More precisely update the
    structure:

        [#correct, #incorrect, #show]
    """

    # Obtain the fields out of the event
    question_id = payload.get('question_id', None)
    answer = payload.get('answer', None)

    # Check that the event is properly formed
    if question_id is None or answer is None:
        raise Exception('MCQ event with incorrect format ' +
                        json.dumps(payload))

    user_i = get_indicator(user, 'EVC_MCQ', ctx_object, question_id, version)

    # Load the JSON data for both indicators
    counters = json.loads(user_i.payload)

    answer = int(answer)  # Value -1 is sent as a string instead of an integer

    # Answer = 0 : Incorrect
    # Answer = 1 : Correct
    # Answer = -1: Show
    if answer == 0:
        counters[1] += 1
    elif answer == 1:
        counters[0] += 1
    else:
        counters[2] += 1

    # Update in DB
    user_i.payload = json.dumps(counters)
    user_i.updated = timezone.now()
    user_i.save()


def update_resource_view(version, user, ctx_object, payload):
    """

    :param version:
    :param user: user to update
    :param ctx_object: context
    :param payload: Event payload such as, for example

           {"url":"https://flip.ee.usyd.edu.au/elec1601/"}

    :return: Update the indicators in the DB with the following structure:

             #number of hits in the url
    """

    url_str = payload.get('url', None)
    if url_str is None:
        raise Exception('RESOURCE_VIEW event with incorrect format ' +
                        json.dumps(payload))

    url_path = urlparse(url_str)

    user_i = get_indicator(user,
                           'EVC_RESOURCE_VIEW',
                           ctx_object,
                           url_path.path,
                           version)

    # Access the counter and increase
    user_counter = json.loads(user_i.payload)
    user_counter += 1

    # Update information in DB
    user_i.payload = json.dumps(user_counter)
    user_i.updated = timezone.now()
    user_i.save()


def update_video(version, user, ctx_object, payload):
    """

    :param version:
    :param user: user to update
    :param ctx_object: context
    :param payload: Event payload such as, for example

     {"time":0,"id":"F0Ri2TpRBBg","event":"PLAY"}

    :return: Update the indicators in the DB. More precisely update the
    structure:

        [#play, #pause, #loaded, #end, #secs, #last-time-played]
    """

    # Obtain the fields out of the event
    video_id = payload.get('id', None)
    video_time = payload.get('time', None)
    event_type = payload.get('event', None)

    # Check that the event is properly formed
    if video_time is None or video_id is None or event_type is None:
        raise Exception('VIDEO event with incorrect format ' +
                        json.dumps(payload))

    user_i = get_indicator(user, 'EVC_VIDEO', ctx_object, video_id, version)

    # Load the JSON data from the indicator
    counters = json.loads(user_i.payload)

    if event_type == 'PLAY':
        counters[0] += 1
        counters[5] = video_time
    elif event_type == 'PAUSED' or event_type == 'END':
        if event_type == 'PAUSED':
            counters[1] += 1
        else:
            counters[3] += 1
        viewed = video_time - counters[5]
        if viewed < 1200:
            # We only take into account if intervals are less than 20 mins
            counters[4] += viewed
        # Reset the last time played counter
        counters[5] = 0
    elif event_type == 'LOADED':
        counters[2] += 1
        counters[5] = 0
    else:
        raise Exception('Unexpected event type in video event' +
                        json.dumps(payload))

    # Update in DB
    user_i.payload = json.dumps(counters)
    user_i.updated = timezone.now()
    user_i.save()


def update_form_submit(version, user, ctx_object, payload):
    """

    :param version:
    :param user: user to update
    :param ctx_object: context
    :param payload: Event payload such as, for example

     {"form_id":"mslq-spq-survey",.......}

    If the payload does not have 'form_id' key, it is ignored.

    :return: Update the value of the indicators in the DB.
    """

    # Obtain the fields out of the event
    form_id = payload.get('form_id', None)

    # Check that the event is properly formed
    if form_id is None:
        print('Form-submit event with incorrect format '
              + json.dumps(payload), file=sys.stderr)
        return

    user_i = get_indicator(user,
                           'EVC_FORM_SUBMIT',
                           ctx_object,
                           form_id,
                           version)

    # Load the JSON data from the indicator
    user_counter = json.loads(user_i.payload)
    user_counter += 1

    # Update information in DB
    user_i.payload = json.dumps(user_counter)
    user_i.updated = timezone.now()
    user_i.save()

    # Consider special cases of forms to process
    if form_id == 'spq-survey':
        print('Processing SPQ form submission', file=sys.stderr)

        questions = ["spq_q{0:02}".format(x) for x in range(1, 21)]
        # Get the answers from the payload or zero if there is none
        answers = [int(payload.get(x, 0)) for x in questions]


        # (DA: Deep approach,
        #  SA: Surface approach,
        #  DM: Deep motive,
        #  SM: Surface motive,
        #  DS: Deep strategy,
        #  SS: Surface strategy)
        result = dict(zip(['SPQ_DA', 'SPQ_SA', 'SPQ_DM', 'SPQ_SM',
                           'SPQ_DS', 'SPQ_SS'],
                          spq_encode(answers)))
        user_mgmt.user_operations.create_or_update_user_context_record(
            user.email,
            ctx_object.name,
            result)


def update_xyclick(user, ctx_object, payload):
    """

    :param user: user to update
    :param ctx_object: context
    :param payload: Event payload such as, for example

    {"id":"ISA-subsetsummary-videoeqt",
     "url":"http://127.0.0.1:8000/..../ISA_subsetsummary_videoeqt.html",
     "title":"ACTIVITY TITLE - COURSE TITLE",
     "x":40,
     "y":57}

    :return: Update the indicators in the DB. More precisely update the
    structure:

        [#play, #pause, #loaded, #end, #secs, #last-time-played]
    """

    # Obtain the fields out of the event
    xy_id = payload.get('id', None)
    xy_url = payload.get('url', None)
    xy_title = payload.get('title', None)
    xy_xcoord = payload.get('x', None)
    xy_ycoord = payload.get('y', None)

    # This indicator does not need to be differentiated betweeen versions. So
    # bypass its value
    version = 0

    # Check that the event is properly formed
    if xy_id is None or xy_url is None or xy_title is None \
            or xy_xcoord is None or xy_ycoord is None:
        raise Exception('XYCLICK event with incorrect format ' +
                        json.dumps(payload))

    # Get or create indicator. The resource_id is "-" because there is no need
    # to distinguish data within the map. All 4 maps are folded in the
    # payload
    user_i = get_indicator(user, 'EV_XY_MAP', ctx_object, "-", version)

    # Load the JSON data from the indicator (list with four maps)
    xy_maps = json.loads(user_i.payload)

    # Encode quadrant: Starting from upper right and counter clock wise
    try:
        xy_xcoord = int(xy_xcoord)
        xy_ycoord = int(xy_ycoord)
    except ValueError:
        # Payload has been manipulated and contains an invalid integer. Skip this event
        return
    
    if xy_xcoord >= 0:
        if xy_ycoord >= 0:
            quadrant = 0
        else:
            quadrant = 3
    else:
        if xy_ycoord >= 0:
            quadrant = 1
        else:
            quadrant = 2

    # Trim the suffix in the title because if it is generated by Sphinx it has
    # the title of the course as suffix.
    pass

    # Add a parameter to the URL to detect click through the study kit
    parsed = urlparse(xy_url)
    qd = parse_qs(parsed.query, keep_blank_values=True)
    qd['study-kit'] = 't'
    xy_url = urlunparse([parsed.scheme,
                         parsed.netloc,
                         parsed.path,
                         parsed.params,
                         urlencode(qd, doseq=True),
                         parsed.fragment])

    # Update the four quadrants. Update content or delete
    for qid in range(0, 4):
        if quadrant == qid:
            # Insert the new element
            xy_maps[qid][xy_id] = [xy_title, xy_url]
        else:
            # Remove the element
            xy_maps[qid].pop(xy_id, None)

    # Update the indicator in the user dictionary.
    user_i.payload = json.dumps(xy_maps)
    user_i.updated = timezone.now()
    user_i.save()
