# Data2u

Data2u is a toolkit to process data obtained from a learning environment. It is designed to be used with the [ReAuthoring](https://bitbucket.org/abelardopardo/reauthoring) toolkit.

Follow the instructions below to get set up.

## Installation Instructions

### 0) Clone this project

Make sure you have git installed, and clone this project with:

```
git clone https://bitbucket.org/comp3615p11/data2u.git
```

### 1) Install Python 3 and pip

To get started, you'll need to make sure you have Python 3 and a recent version of pip, the python package manager, installed.

* [Install Python 3](https://www.python.org/downloads/release/python-362/)
* [Install pip](https://pip.pypa.io/en/stable/installing/) (already installed if you downloaded a binary from Python.org)

### 2) Install requirements

Navigate into the cloned project folder `data2u`, and automatically install all the requirements with pip:

```
pip install -r "requirements.txt"
```

### 3) Run data2u

Now you are ready to run data2u!
