#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from django.contrib import admin

from leco.models import Submission, Action, UserGroup, ActionGroup, \
    UploadedFile


class LecoAdminSubmission(admin.ModelAdmin):
    date_hierarchy = 'received'
    list_display = ('id', 'context', 'user', 'action', 'payload', 'received', 
                    'http_host',  'remote_user', 'remote_addr', 'server_name', 
                    'server_port')
    # list_filter = ['context', 'user', 'action', 'payload', 'received', 
    #                'http_host', 'remote_user']
    search_fields = ['user__email', 'action__name', 'context', 'http_host',
                     'remote_user']


class LecoAdminAction(admin.ModelAdmin):
    list_display = ('name', 'creator', 'accept_from', 'accept_until')


class LecoAdminUploadedFile(admin.ModelAdmin):
    list_display = ('logical_name', 'physical_name')
    list_filter = ['logical_name', 'physical_name']
    search_fields = ['logical_name', 'physical_name',
                     'submission__action__name']


class LecoAdminActionGroup(admin.ModelAdmin):
    filter_horizontal = ('actions',)


class LecoAdminUserGroup(admin.ModelAdmin):
    filter_horizontal = ('users',)

admin.site.register(Submission, LecoAdminSubmission)
admin.site.register(UserGroup, LecoAdminUserGroup)
admin.site.register(Action, LecoAdminAction)
admin.site.register(UploadedFile, LecoAdminUploadedFile)
admin.site.register(ActionGroup, LecoAdminActionGroup)
