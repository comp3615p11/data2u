#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import pytz

from django.db import models
from django.conf import settings

import user_mgmt.models

this_tz = pytz.timezone(settings.TIME_ZONE)


#
# Table with version id and comments to send to that version
#
class Suggestion(models.Model):

    # Name of this suggestion. Must be unique
    name = models.CharField(max_length=256, primary_key=True, unique=True)

    # HTML snippet with the suggestion.
    feedback = models.TextField()

    def __str__(self):
        return self.name


#
# Table with user id, version assigned to the user, and open text for
# additional feedback
# 
class UserSuggestion(models.Model):

    # User for which the suggestion is required
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)

    # Context in which the suggestion is relevant
    context = models.ForeignKey(user_mgmt.models.Context,
                                on_delete=models.CASCADE)

    # Pointer to the suggestion
    suggestion = models.ForeignKey(Suggestion,
                                   on_delete=models.CASCADE)

    # Datetime from which suggestion is present
    s_from = models.DateTimeField('From', auto_now=True)

    # Datetime until which suggestion is considered
    s_until = models.DateTimeField('Until')

    def __str__(self):
        return '%s: %s' % (self.user, self.suggestion)

    class Meta:
        verbose_name = "user suggestion"
