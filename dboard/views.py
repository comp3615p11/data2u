#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import json

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from future.moves.urllib.parse import urlparse, parse_qsl, urlunparse, urlencode

import accumulate.models
import dboard.forms
import dboard.models
import leco.views
import user_mgmt.models
import user_mgmt.user_operations

# For Python 3
# import urllib.request, urllib.parse, urllib.error
# import urllib.parse


@login_required
def getdb(request, context_name):
    """
    :param request: Object with the HTML request
    :param context_name: Context in which the user is operating

    Serve the dashboard. Customizable with the following pairs:

    - data_capture_url: URL to send the events captured by the JS. Default ''
    - data_capture_method: 'POST',
    - data_capture_subject_name: field name for data capture,
    - data_capture_verb_name:    field name for data capture,
    - data_capture_object_name:  field name for data capture,
    - data_capture_context_name: field name for data capture,
    """

    # Load the context
    # noinspection PyBroadException
    try:
        ctx_handler, ctx_object = \
            user_mgmt.models.load_context_module(context_name)
    except Exception as e:
        print(e)
        message = "Page invoked with non-existent context %s." \
                  % context_name
        # message += "<br/><p>" + e.message + "</p>"
        # traceback.print_exc(file=sys.stdout)
        return render(request,
                      'severe_error.html',
                      {'message': message})

    # Get some initial common variables from the request and the context
    # handler
    versions = [(x, x) for x in range(ctx_handler.version_range[0],
                                      ctx_handler.version_range[1])]
    user_id = request.user.email

    # Get the dictionary to use when rendering the page
    render_dict = ctx_handler.render_dict

    # If the request is a get, get the simple form
    version = None
    if request.method == 'GET':
        if request.user.is_superuser:
            # New entry for superuser
            form = dboard.forms.DboardSuperUserForm(user_id, 0, versions)
        else:
            # New entry for a regular user,
            form = dboard.forms.UserForm(0, versions)
    # The page is loaded after a POST request
    else:
        version = request.POST.get('version', None)
        if request.user.is_superuser:
            submitted_user = request.POST.get('user', None)
            if submitted_user is None or version is None:
                message = 'Page invoked incorrectly (empty parameters).'
                return render(request,
                              'severe_error.html',
                              {'message': message})

            form = dboard.forms.DboardSuperUserForm(submitted_user,
                                                    version,
                                                    versions)
            user_id = submitted_user
        else:
            if version is None:
                message = 'Page invoked incorrectly (empty parameters).'
                return render(request,
                              'severe_error.html',
                              {'message': message})

            form = dboard.forms.UserForm(version, versions)

    # Set the form and the username to use in this page
    render_dict['form'] = form
    render_dict['user_id'] = user_id

    # Get the version (in case it is not provided by the URL or as a
    # parameter)
    if version is None:
        version = ctx_handler.get_version() + 1
    else:
        version = int(version)

    # Bypass incorrect arguments
    if version < ctx_handler.version_range[0] or \
            version >= ctx_handler.version_range[1]:
        version = ctx_handler.get_version() + 1

    # Set the time slot to show the version number
    render_dict['time_slot'] = version

    # Once user and week has been selected, get the data and render
    return render_db(request, user_id, ctx_handler)


def render_db(request, username, ctx_handler):
    # Get the screen number as the parameter SN, zero by default
    screen_number = request.GET.get('sn')

    if screen_number == "1":
        return render_study_kit(request, username, ctx_handler)
    elif screen_number == "2" and request.user.is_superuser:
        return render_student_report(request, username, ctx_handler)

    return render_dashboard_engagement(request, username, ctx_handler)


def render_student_report(request, username, ctx_handler):
    # Fetch the render dictionary
    render_dict = ctx_handler.render_dict

    # Fetch the context name
    context_name = ctx_handler.context_name
    render_dict['context_name'] = context_name

    # Update the render_dict using the method in the class
    ctx_handler.update_render_dict(username)

    # SID, First name, Last name
    p_record = user_mgmt.models.PersonalRecord.objects.filter(
        user__email=username,
    )

    render_dict['no_data'] = len(p_record) == 0

    # If there are no personal records, render and return
    if len(p_record) == 0:
        return render(request, render_dict['template_sr'], render_dict)

    indicators = accumulate.models.Indicator.objects.filter(
        user__email=username,
        version=int(render_dict['time_slot']) - 1,
    )

    ctx_handler.populate_student_report(p_record[0], indicators, render_dict)

    return render(request, render_dict['template_sr'], render_dict)


def render_dashboard_engagement(request, username, ctx_handler):
    # Fetch the render dictionary
    render_dict = ctx_handler.render_dict

    # Fetch the context name
    context_name = ctx_handler.context_name
    render_dict['context_name'] = context_name

    # Fetch time slot from the dictionary
    version = render_dict['time_slot'] - 1

    # Fetch the date/time of the last update and show it
    try:
        filter_footprint = accumulate.models.FilterFootprint.objects.get(
            context__name=context_name,
            filter_name='calculate_week_scores')
        render_dict['last_updated'] = filter_footprint.datetime_last_processed
    except ObjectDoesNotExist:
        render_dict['last_updated'] = 'No data available'

    # Fetch the value of the show_dboard flag from the personal record
    # and insert it in the dictionary.
    render_dict['show_dboard'] = \
        user_mgmt.user_operations.get_personalrecord_value(
            username, 'd2u_show_dboard_form_show-dboard', 'Yes'
        )

    # Fetch the value of show_suggestions flag from the personal record
    # and insert it in the dictionary
    render_dict['show_suggestions'] = \
        user_mgmt.user_operations.get_personalrecord_value(
            username,
            'd2u_show_suggestions_form_show-suggestions',
            'Yes'
        )

    # Update the render_dict using the method in the class
    ctx_handler.update_render_dict(username, version)

    #
    # Register the event with LECO
    #
    _ = leco.views.populate_submission(
        request.user,
        leco.views.find_or_add_action('dboard-view'),
        {leco.views.context_field_name: context_name,
         leco.views.payload_field_name:
             json.dumps({'week': version,
                         'user': username,
                         'values': render_dict['dboard_data']})
         },
        request.META)

    #
    # Fetch the suggestions
    suggestions = get_suggestion_data(username, version, context_name)
    if suggestions:
        render_dict['suggestion_data'] = suggestions

    return render(request, render_dict['template'], render_dict)


def render_study_kit(request, username, ctx_handler):
    """
    :param request: Request element received from the browser
    :param username: Username of whoever requested this
    :param ctx_handler: Context handler object
    :return: Renders page

    Method that renders the study kit page. The steps are:

    - Get the render dictionary
    - Get the Map string from the indicators for the user (single element)
    - Set the length of each of the four categories to zero
    - If there is information in the map:
      - Render the length of that category
      - Create the <ul> with the <li> for each of the links in sub-maps
    """
    # Fetch the render dictionary
    render_dict = ctx_handler.render_dict

    # Fetch the context name
    context_name = ctx_handler.context_name
    render_dict['context_name'] = context_name

    # Update the render_dict using the method in the class
    ctx_handler.update_render_dict(username)

    # Fetch the user map and parse its JSON payload
    user_map_string = accumulate.models.Indicator.objects.filter(
        user__email=username,
        type='EV_XY_MAP').values_list('payload')

    # Fetch the date/time of the last update and show it
    try:
        filter_footprint = accumulate.models.FilterFootprint.objects.get(
            context__name=context_name,
            filter_name='update_event_counters')
        render_dict['last_updated'] = filter_footprint.datetime_last_processed
    except ObjectDoesNotExist:
        render_dict['last_updated'] = 'No data available'

    # Initialise the counters for each section to zero (shown in page)
    render_dict['xy_helped_easy_n'] = 0
    render_dict['xy_helped_difficult_n'] = 0
    render_dict['xy_confused_difficult_n'] = 0
    render_dict['xy_confused_easy_n'] = 0

    # If the is a map to process, proceed.
    if len(user_map_string) != 0:
        user_map = json.loads(user_map_string[0][0])

        # Process the quadrants to generate the appropriate HTML snippets
        quadrant_key_names = ['xy_helped_easy', 'xy_helped_difficult',
                              'xy_confused_difficult', 'xy_confused_easy']
        for qid in range(0, 4):

            # Update the length of the section
            render_dict[quadrant_key_names[qid] + '_n'] = len(user_map[qid])

            # Transform the items in the dictionary in an itemized list
            list_html = ['<a href="' + url_string + '">' + title + '</a>'
                         for (title, url_string) in
                         sorted(user_map[qid].values())]

            # If there is no activity in the map, insert Message. Otherwise
            # add the <li></li> labels and the wrapping <ul>
            if user_map[qid] == {}:
                list_html = "<p>No activities labeled in this category</p>"
            else:
                list_html = '<ul><li>' + '</li><li>'.join(list_html) + \
                            '</li></ul>'

            # Insert the string in the right quadrant variable
            render_dict[quadrant_key_names[qid]] = list_html

    #
    # Register the event with LECO
    #
    _ = leco.views.populate_submission(
        request.user,
        leco.views.find_or_add_action('studykit-view'),
        {leco.views.context_field_name: context_name,
         leco.views.payload_field_name:
             json.dumps({'user': username,
                         'values': [render_dict['xy_helped_easy_n'],
                                    render_dict['xy_helped_difficult_n'],
                                    render_dict['xy_confused_difficult_n'],
                                    render_dict['xy_confused_easy_n']]})
         },
        request.META)

    return render(request, render_dict['template_sk'], render_dict)


def get_suggestion_data(username, version, context):
    """
    :param username: Name of the user to search for commenbts
    :param version: Version number to be retrieved
    :param context: Fetch the comments from this context

    Get from the User_suggestion table the user. Combine the feedback in that
    table with the feedback from the version, if assigned. Finally, get all the
    suggestions from the Suggestions table.

    :return The set of suggesionts (to be implemented)
    """

    cmt = accumulate.models.Indicator.objects.filter(
        user__email=username,
        version=version,
        context__name=context,
        type="CMT_YOURSTRATEGY"
    ).values_list('payload')

    if len(cmt) == 0:
        return None

    # The comments could be a simple string, or a list with two elements.
    # If it is a list with two elements, it needs to be transformed to a
    # <a href="{2}">{1}</a> format.
    comments = json.loads(cmt[0][0])['text']
    result = []
    for item in comments:
        if type(item) is list:
            url_bits = urlparse(item[1])
            # Add the param to the URL
            query_dict = dict(parse_qsl(url_bits.query))
            query_dict['_dblnk_'] = 1
            item[1] = urlunparse((
                url_bits.scheme,
                url_bits.netloc,
                url_bits.path,
                url_bits.params,
                urlencode(query_dict),
                url_bits.fragment
            ))
            item = '<a href="{0}">{1}</a>'.format(item[1], item[0])

        result.append(item)

    return result

    # if len(user_suggestions) == 0:
    #     return
    #
    # return '<ul><li>' + \
    #     '</li><li>'.join([x.suggestion.feedback for x in user_suggestions]) \
    #     + '</li></ul>'
