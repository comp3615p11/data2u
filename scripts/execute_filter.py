#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import getopt
import inspect
import shlex
import sys
import time
import traceback
import types

from django.utils import timezone

import accumulate.iepf
import accumulate.models
import data2u.settings
import user_mgmt
import user_mgmt.models

filter_catalog = {
    # <filter_name>: filter_function(event, version)
    'update_event_counters': accumulate.iepf.update_event_counters,
}

__doc__ = """
"""


def execute_filters(context_name, filter_names, filter_options):
    """
    :param context_name: Name of the context to select the events
    :param filter_names: Filters to execute
    :param filter_options: Dictionary with options to run the filters
    :return:

    Pseudocode:

    - Given a set of filter names and options, loop over each filter
      and
        - Loads an external file containing a class inheriting from
          Handler_basic (part of the course file)
        - Finds where the function corresponding with the filter name is
          located.
        - Invoke that function
    """

    global filter_catalog

    # Measure execution time
    start_time = time.time()

    print('BEGIN:', end=' ', file=sys.stderr)
    print(timezone.now().strftime("%Y-%m-%d %H:%M:%S %Z"), file=sys.stderr)

    # Load the given context module handler
    ctx_handler, ctx_object = user_mgmt.models.load_context_module(context_name)

    # Process the list of filter names and check if they are either valid
    # with respect to the list at the top of this file, or they are members
    # of the context handler
    filter_pairs = []
    for filter_name in filter_names:

        # Look up in the default catalog
        filter_function = filter_catalog.get(filter_name, None)
        if filter_function is not None:
            # Function is in the filter catalog
            filter_pairs.append((filter_name, filter_function))
            continue

        # Look for the filter name in the context object
        ctx_members = inspect.getmembers(ctx_handler,
                                         predicate=inspect.ismethod)
        filter_function = [item[1] for item in ctx_members
                           if item[0] == filter_name]

        # If no member has been found, notify and stop
        if not filter_function:
            print('Filter', filter_name, 'not found.', file=sys.stderr)
            sys.exit(2)
        filter_pairs.append((filter_name, filter_function[0]))

    # Loop over filter names and execute each of them
    for filter_name, filter_func in filter_pairs:

        # Get the last execution of the filter
        last_execution, created = \
            accumulate.models.FilterFootprint.objects.get_or_create(
                context=ctx_object, filter_name=filter_name)
        if created:
            print('Filter', filter_name, end=' ', file=sys.stderr)
            print('executing for the first time', file=sys.stderr)
        else:
            print('Filter', filter_name, 'last executed in', end=' ',
                  file=sys.stderr)
            print(last_execution.datetime_last_processed, file=sys.stderr)

        # Execute the filter depending if it is an object method or not
        try:
            if isinstance(filter_func, types.FunctionType):
                # Object function, execute with the context handler
                filter_func(ctx_object, ctx_handler, last_execution,
                            filter_options)
            else:
                # Standalone function
                filter_func(ctx_object, last_execution, filter_options)
        except Exception:
            print('Exception in filter', filter_name, file=sys.stderr)
            print('** STACK-BEGIN-', file=sys.stderr)
            traceback.print_exc(file=sys.stderr)
            print('** STACK-END-', file=sys.stderr)

    # Elapsed time
    elapsed_time = time.time() - start_time
    print('Elapsed time:', elapsed_time, 'seconds.', file=sys.stderr)
    print('END:', end=' ', file=sys.stderr)
    print(timezone.now().strftime("%Y-%m-%d %H:%M:%S %Z"), file=sys.stderr)


def run(*script_args):
    """
    Script to apply a filter to the events collected so far.

    The options for the script are:
    -d : Turns on debugging messages

    -l <number> : maximum number of events to process

    -f <name>: name of the filter to apply as registered at the top of this
               file

    -o name=value: Pairs (multiple options allowed) passed to the filter

    <context_name> The name of the context to process. The database must
    contain a context with this name and an absolute path to a file that
    defines a class inheriting from Handler_basic defining a set of fields
    required for event processing (see the file accumulate/__init__.py).

    Execute as:

    $ ./manage.py runscript -v 3 --traceback \
       execute_filter \
       --settings=data2u.settings \
       --script-args="-f update_event_counters \
                      -f calculate_week_scores \
                      -f create_comments ELEC1601"

    """

    global filter_catalog

    # Parse the arguments
    argv = shlex.split(script_args[0])

    filter_names = []
    filter_options = {}
    try:
        opts, args = getopt.getopt(argv, "dhf:o:")
    except getopt.GetoptError as e:
        print(e.msg)
        print(run.__doc__)
        sys.exit(2)

    # Process the arguments
    for optstr, value in opts:
        if optstr == "-d":
            data2u.settings.DEBUG = True
        elif optstr == "-f":
            filter_names.append(value)
        elif optstr == "-o":
            try:
                n, v = value.split('=')
            except ValueError:
                print('Option "{0}" is incorrect'.format(value))
                print('Options should be name=value')
                sys.exit(2)
            filter_options[n] = v
        elif optstr == "-h":
            print(run.__doc__)
            sys.exit(0)

    # The script must have at least a filter
    if not filter_names:
        print('The script needs to specify at least a filter with option -f')
        sys.exit(2)

    # Process the remaining arguments
    if len(args) != 1:
        print("The script needs a context name.")
        sys.exit(2)

    # Invoke all the filters
    execute_filters(args[0], filter_names, filter_options)
