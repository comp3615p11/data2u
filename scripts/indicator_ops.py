#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import codecs
import csv
import getopt
import json
import shlex
import sys

import django.contrib.auth
from django.utils import timezone

import accumulate.models
import user_mgmt.models
import user_mgmt.user_operations


def indicator_filter(options):
    """

    :param options: Object containing all the options for the commands
    :return: Set of indicators filtered by the options
    """

    # Initial query set with all the indicators for the given context
    queryset = accumulate.models.Indicator.objects.filter(
        context__name=options['context_name']
    )

    # First filter, but key/value
    key_name = options['key_name']
    value = options['value']
    if key_name == 'user_id':
        queryset = queryset.filter(user__id=value)
    elif key_name == 'email':
        queryset = queryset.filter(user__email=value)
    elif key_name is None and value is not None:
        queryset = queryset.filter(payload__contains=value)

    # Filter by type
    if options['ind_type'] is not None:
        queryset = queryset.filter(type=options['ind_type'])

    # Filter by resource_id
    if options['resource_id'] != '':
        queryset = queryset.filter(resource_id=options['resource_id'])

    # Filter, by version
    if options['version'] != -1:
        queryset = queryset.filter(version=options['version'])

    return queryset


def insert_indicator(user, ind_type, context_name, version, resource_id,
                     payload, debug):
    """
    Insert or update the payload of an indicator with the given parameters.

    :param user: User object to select (could be None)
    :param ind_type: type of indicator to insert
    :param context_name: context name to store in the indicator
    :param version: version number to store in the indicator
    :param resource_id: resource ID to store in the indicator
    :param payload: Payload to store in the indicator
    :param debug: Flag to enable debug messages
    :return: Create or update the indicator element in the database
    """

    # See if the indicator exists
    if debug:
        print('Inserting indicator with:', file=sys.stderr)
        print('    user:', user, file=sys.stderr)
        print('    ind_type:', ind_type, file=sys.stderr)
        print('    context_name:', context_name, file=sys.stderr)
        print('    version:', version, file=sys.stderr)
        print('    resource_id:', resource_id, file=sys.stderr)

    try:
        indicator = accumulate.models.Indicator.objects.get(
            user=user,
            type=ind_type,
            context__name=context_name,
            version=version,
            resource_id=resource_id
        )
    except accumulate.models.Indicator.DoesNotExist:
        ctx = user_mgmt.models.Context.objects.get(name=context_name)
        indicator = accumulate.models.Indicator(
            user=user,
            type=ind_type,
            context=ctx,
            version=version,
            resource_id=resource_id,
            updated=timezone.now(),
            payload=payload
        )
        indicator.save()

        if debug:
            print('Indicator created.', file=sys.stderr)

        return

    # The indicator IS in the DB, update and notify
    print('Indicator in the DB. Updating payload and timestamp', file=sys.stderr)
    indicator.payload = payload
    indicator.updated = timezone.now()


def remove_indicator_cmd(options):
    """
    :param options: Dictionary with the options
    :return: None. Effect reflected in the DB

    Deletes the indicators that are obtained by filtering by:

    - user_id = value (if key = user_id)
    - email = value (if key = email)
    - payload contains value (if key = None)
    - type = ind_type if type is given in options
    - resource_id = resource_id if resource_id is given in options
    - version = version if version is given in options
    """

    # Get the set of indicators filtered by the options
    queryset = indicator_filter(options)

    if options['debug']:
        print('Deleting', len(queryset), 'indicators.', file=sys.stderr)

    # Proceed to delete the object"
    queryset.delete()


def insert_indicator_cmd(options):
    """
    :param options: Dictionary with the options
    :return: None. Effect reflected in the DB

    """

    # Get some options
    key_name = options['key_name']
    value = options['value']

    # Look up the user
    if key_name == 'user_id' and value is not None:
        user = user_mgmt.user_operations.user_lookup_by_id(value)
    elif key_name == 'email' and value is not None:
        user = user_mgmt.user_operations.user_lookup_by_email(value)
    else:
        raise Exception('Incorrect key_name' + key_name)

    insert_indicator(user,
                     options['ind_type'],
                     options['context_name'],
                     options['version'],
                     options['resource_id'],
                     options['payload'],
                     options['debug'])


def insert_indicator_to_all_cmd(options):
    """

    :param options: Dictionary with the options
    :return: None. Effect reflected in the DB
    """

    # Obtain the UIDs of the members of the context
    all_users = user_mgmt.models.Context.objects.filter(
        name=options['context_name']).values_list('members', flat=True)

    # Loop over all user_ids and insert the indicator
    for user in all_users:
        insert_indicator(user,
                         options['ind_type'],
                         options['context_name'],
                         options['version'],
                         options['resource_id'],
                         options['payload'],
                         options['debug'])


def update_from_csv_file_cmd(options):
    """
    :param options: Dictionary with the options
    :return: None. Effect reflected in the DB
    """

    # Open the file for reading
    file_in = codecs.open(options['file_name'], 'rU')
    dialect = csv.Sniffer().sniff(file_in.read(1024))
    file_in.seek(0)
    data_in = csv.reader(file_in, dialect)

    # Loop over the lines
    line_number = 0
    header_detected = False
    header_mark = options['header_mark']
    key_name = options['key_name']
    column_names = options['column_names']
    for data in data_in:
        # Count the line number to flag anomalies
        line_number += 1

        # If mark has not been detected yet
        if not header_detected:
            if header_mark is not None and header_mark not in data:
                # Mark is given, but line does not match, skip it
                continue

            # At this point either the mark has been detected, or no mark has
            # been given, in which case, still take it as detected
            header_detected = True

            # See if key_name column is part of the file
            if key_name not in data:
                print('Column', key_name, 'not found.', file=sys.stderr)
                print('Columns:', data, file=sys.stderr)
                sys.exit(1)
            else:
                key_name_idx = data.index(key_name)

            # Traverse the list of columns not in header and flag.
            missing_columns = [x for x in column_names if x not in data]
            if len(missing_columns) != 0:
                print('Columns not found in file: ', \
                    missing_columns, file=sys.stderr)
                sys.exit(1)

            column_names_idx = [data.index(x) for x in column_names]

            if options['debug']:
                print('Columns:', data, file=sys.stderr)
                print('Lookup idx:', key_name_idx, file=sys.stderr)
                print('Select idx:', column_names_idx, file=sys.stderr)

            # Proceed with the following lines
            continue

        # Safety check. If something went wrong when the CSV file was
        # exported, it is very likely that the string #REF! is present. If
        # so, notify and stop.
        if '#REF!' in data:
            print('Line', line_number, 'contains incorrect data', file=sys.stderr)
            sys.exit(1)

        # If any of the two indexes is not legal in this line, skip it.
        if key_name_idx >= len(data) or max(column_names_idx) >= len(data):
            print('Mismatched line', line_number, file=sys.stderr)
            continue

        # At this point we are processing a data line
        user = None
        if data[key_name_idx] != '':
            user = user_lookup_by_pair(
                options['compare_attr'],
                data[key_name_idx],
                options['context_name']
            )

        if user is None:
            # User has not been found. Skip the line.
            print('User', data[key_name_idx], 'not found.', end=' ', file=sys.stderr)
            print('Skipping', file=sys.stderr)
            continue

        # Insert the indicator
        insert_indicator(user,
                         options['ind_type'],
                         options['context_name'],
                         options['version'],
                         options['resource_id'],
                         json.dumps(dict(list(zip(column_names,
                                             [data[x]
                                              for x in column_names_idx])))),
                         options['debug'])


def dump_csv_file_cmd(options):
    """
    :param options: Dictionary with the options
    :return: None. Effect reflected in the DB
    """

    # Create the csv writer object
    data_out = open(options['file_name'], 'wb')
    csv_out = csv.writer(data_out, delimiter=',', quotechar='"')

    # Get the set of indicators filtered by the options
    queryset = indicator_filter(options)

    # To count and notify advance when processing the indicators
    num_ind = len(queryset)
    lap = num_ind / 10
    print(num_ind, 'indicators to process.')

    # Get the column names and accumulate the dictionaries for each user
    user_dicts = {}

    # Loop over the indicators
    count = 0
    for indicator in queryset:

        count += 1
        if count == lap:
            count = 0
            print('STEP')

        # Get the user
        user = indicator.user
        if user is None:
            user_email = ''
        else:
            user_email = user.email

        # Get or create the dictionary with the row data
        user_row = user_dicts.get(user_email, None)
        if user_row is None:
            user_row = {}
            user_dicts[user_email] = user_row

        # Create the prefix for the column based on the type, the resource_id
        #  and the version.
        itype = indicator.type
        prefix = '_'.join([itype,
                           indicator.resource_id,
                           str(indicator.version)])
        # Some post-processing for the column name is required.
        if itype == 'EVC_COL_EXP':
            pass
        elif itype == 'EVC_COL_EXP':
            pass
        elif itype == 'EVC_DURATION':
            pass
        elif itype == 'EVC_RESOURCE_VIEW':
            pass
        elif itype == 'EVC_EXCO':
            # Get the payload
            payload = json.loads(indicator.payload)

            ex_stats = payload[1]
            cor = sum([y[0] for x, y in list(ex_stats.items())])
            inc = sum([y[1] for x, y in list(ex_stats.items())])
            user_row[prefix + '_COR'] = cor
            user_row[prefix + '_INC'] = inc
        elif itype == 'EV_XY_MAP':
            pass
        elif itype == 'EVC_DBOARDVIEW':
            pass
        elif itype == 'EVC_FORM_SUBMIT':
            pass
        elif itype == 'EVC_PIAZZA':
            pass
        elif itype == 'EVC_VIDEO':
            # Get the payload
            payload = json.loads(indicator.payload)
            user_row[prefix + '_PLAY'] = payload[0]
            user_row[prefix + '_PAUSE'] = payload[1]
            user_row[prefix + '_SECS'] = payload[4]
        elif itype == 'CMT_YOURSTRATEGY':
            pass
        elif itype == 'EVC_MCQ':
            # Get the payload
            payload = json.loads(indicator.payload)
            user_row[prefix + '_COR'] = payload[0]
            user_row[prefix + '_INC'] = payload[1]
        elif itype == 'SCORE':
            pass
        else:
            print('Indicator type', itype, 'unknown', file=sys.stderr)
            # sys.exit(1)

    if options['debug']:
        print(len(user_dicts), 'user rows to process', file=sys.stderr)

    column_names = set([])
    for user_email, user_row in list(user_dicts.items()):
        column_names = column_names.union(list(user_row.keys()))

    if options['debug']:
        print(len(column_names), 'columns to process', file=sys.stderr)

    # Sort the list of columns
    column_names = list(column_names)
    column_names.sort()

    if options['debug']:
        print('Dumping', len(user_dicts), 'by', \
            len(column_names), 'matrix', file=sys.stderr)

    # Write the column names
    csv_out.writerow(['email'] + column_names)

    # Dump the file
    for user_email, user_row in list(user_dicts.items()):
        csv_out.writerow([user_email] +
                         [user_row.get(x, 0) for x in column_names])


def user_lookup_by_pair(field_name, field_value, context_name):
    """
    :param field_name: Field to use for the lookup (email or user_id)
    :param field_value: Value of the field to use for the lookup
    :param context_name: string encoding the context name
    :return: User_id or None if not found

    Find or add the user with field_name = field_value in the user model and
    add to the context if not present.
    """

    # Raise exception if any of the arguments is None
    if field_name == 'user_id':
        # Special case when looking up by user_id
        user = user_mgmt.user_operations.user_lookup_by_id(field_value)
        if user is None:
            user = user_mgmt.user_operations.activate_account(field_value)
    elif field_name == 'email':
        # Special case to lookup with email
        user = user_mgmt.user_operations.user_lookup_by_email(field_value)
        if user is None:
            user = user_mgmt.user_operations.activate_account_with_email(
                field_value
            )
    else:
        raise Exception('Function needs two valid arguments')

    # Make sure we check that the user is member of the context (with a
    # simple fetch of the user object through context member). If the user is
    #  not member of the context, an exception will be raised.
    user_model = django.contrib.auth.get_user_model()
    user_model.objects.get(id=user.id, contexts__name=context_name)

    return user


def indicator_to_personal_record(options):
    """
    Command that selects a set of indicators and transfers them to the
    PersonalRercord table.

    :param options: Dictionary with all option values
    :return: Nothing. Changes are reflected in the DB

    """

    # Get the set of indicators filtered by the options
    queryset = indicator_filter(options)

    if options['debug']:
        print('Transferring', len(queryset), 'indicators.', file=sys.stderr)

    # Loop over all the indicators
    for indicator in queryset:
        # Find or add the PersonalRecord for the given user and context
        user_record = user_mgmt.user_operations.find_or_add_user_context_record(
            indicator.user,
            options['context_name']
        )
        # Load the dictionary
        user_dict = json.loads(user_record.dictionary)

        key_name = options['ind_type'] + '_'
        if options['resource_id'] != '':
            key_name += '_' + options['resource_id']
        if options['version'] != -1:
            key_name += '_' + str(options['version'])

        value = json.loads(options['payload'])
        if isinstance(value, dict) or isinstance(value, list):
            raise Exception('Indicator has non-simple payload')

        # Update the pair key, value in the dictionary
        user_dict[key_name] = value

        # Save to the database
        user_record.save()


def run(*script_args):
    """
    Operations on the indicators table to handle external data
    sources. More precisely the script can perform the following
    operations:

       -d : Flag to print debug information
       -c <context name>

    - remove_indicator

      Remove all the indicators that match the context, key, value, resource_id,
      version, type.

      The key,value pair can be:
        1) 'user_id', number
        2) 'email', email address
        3) None, value: match the payload with that value

      Options:
      -d
      -k key Key name to use for user lookup (user_id, email, or payload exp)
      -v value Value for the key to select the indicator to remove
      -r resource_id: Resource id to filter (all of them by default)
      -s version: Version number to filter (all of them by default)
      -t type: Indicator type to filter (all of them by default)

    - insert_indicator/update_indicator

      Insert or update an indicator for the user determined by key, value,
      the given context, and the resource_id, version, and type. Update the
      payload with the given value (or insert from scratch if it does not
      exist)

      key = user_id and value = None is used to insert a population indicator.

      Options:
      -d
      -k key Key name to use for user lookup (user_id or email)
      -v value Value to use combined with the key to select user to modify.
               If None, an indicator with empty user is inserted/updated
      -r resource_id: Resource id to filter (all of them by default)
      -s version: Version number to filter (all of them by default)
      -t type: Indicator type to filter (all of them by default)
      -p "payload": String to be evaluated and translated into a
                    JSON object

    - insert_indicator_to_all

      Options:
      -r resource_id: Resource id to filter (all of them by default)
      -s version: Version number to filter (all of them by default)
      -t type: Indicator type to filter (all of them by default)
      -p "payload": String to be evaluated and translated into a JSON
                    object

    - update_from_csv_file

      Options:
      -f filename: CSV filename to process
      -k <column_name>: name of the CSV column with the value for user lookup
      -m string: String to detect the header line
      -n <column_name>: name of the pair to introduce in payload
      -p "payload_column": column name from where to get the payload
      -r resource_id: Resource id to insert
      -s version: Version number to insert
      -t type: Indicator type to insert
      -x <name>: Attribute in the DB to use to lookup user

    - dump_csv_file

      Options:
      -f filename: CSV filename for output
      -k key Key name to use for user lookup (user_id, email, or payload exp)
      -v value Value for the key to select the indicator to remove
      -r resource_id: Resource id to filter (all of them by default)
      -s version: Version number to filter (all of them by default)
      -t type: Indicator type to filter (all of them by default)

      Example:

          ./manage.py runscript -v 3 --traceback \
              --settings=data2u.settings
              indicator_ops
               --script-args="dump_csv_file
                              -c ELEC1601
                              -f mmm.csv"

    - indicator_to_personal_record

      Command to transfer the content of a set of indicators to the personal
      record table. The command creates or updates the records in the
      PersonalRecord model.

      Options
      -k key Key name to use for user lookup (user_id, email or payload exp)
      -v value Value for the key to select the indicator to transfer
      -r resource_id Resource id to filter (all of them by default)
      -s version: Version number to filter (all of them by default)
      -t type: Indicator type to filter (all of them by default)
    """

    # If the script is run with no arguments, print the __doc__.
    argv = shlex.split(script_args[0])
    if len(argv) == 0:
        print(run.__doc__, file=sys.stderr)
        return

    # Default value for the options
    options = {
        # -c <contextName>: Context name for lookup
        'context_name': None,

        # -d No debug information printed
        'debug': False,

        # -f <filename> CSV file to load
        'file_name': None,

        # -k <keyname> Key to identify the user
        'key_name': None,

        # -m <mark> String to help detect the header
        'header_mark': None,

        # -n <string> Column to extract payload
        'column_names': [],

        # -p <json string> To store in payload field
        'payload': '',

        # -r <resource_id> (None by default)
        'resource_id': '',

        # -s <version_number>: Number for indicator lookup
        'version': -1,

        # -t <typeStr>: Type of indicator for lookup
        'ind_type': None,

        # -v <value> Together with key, (key, value) for user lookup
        'value': None,

        # -x <string> Attribute to perform user lookup
        'compare_attr': None,
    }

    # Parse the options
    try:
        opts, args = getopt.getopt(argv[1:], "c:df:k:m:n:p:r:s:t:v:x:")
    except getopt.GetoptError as e:
        print(e.msg, file=sys.stderr)
        print(update_from_csv_file_cmd.__doc__, file=sys.stderr)
        sys.exit(2)

    # Process the arguments
    for optstr, optvalue in opts:
        if optstr == "-c":
            options['context_name'] = optvalue
        elif optstr == "-d":
            options['debug'] = True
        elif optstr == "-f":
            options['file_name'] = optvalue
        elif optstr == "-k":
            options['key_name'] = optvalue
        elif optstr == "-m":
            options['header_mark'] = optvalue
        elif optstr == "-n":
            options['column_names'].append(optvalue)
        elif optstr == "-p":
            options['payload'] = optvalue
        elif optstr == "-r":
            options['resource_id'] = optvalue
        elif optstr == "-s":
            options['version'] = int(optvalue)
        elif optstr == "-t":
            options['ind_type'] = optvalue
        elif optstr == "-v":
            options['value'] = optvalue
        elif optstr == "-x":
            options['compare_attr'] = optvalue

    if options['context_name'] is None:
        print('Option -c <context_name> required', file=sys.stderr)
        print(run.__doc__, file=sys.stderr)
        sys.exit(1)

    # REMOVE INDICATOR COMMAND
    if argv[0] == 'remove_indicator':
        if (options['key_name'] == 'user_id' or
            options['key_name'] == 'email') and \
                        options['value'] is None:
            print('Missing option -v <value>', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        remove_indicator_cmd(options)

    # INSERT INDICATOR COMMAND
    elif argv[0] == 'insert_indicator':

        if options['key_name'] is None:
            print('Missing option -k <value>', file=sys.stderr)
            print(insert_indicator_cmd.__doc__, file=sys.stderr)
            sys.exit(1)

        if options['key_name'] != 'user_id' and options['key_name'] != 'email':
            print('Option -k <value> only allows user_id or ' \
                                 'email', file=sys.stderr)
            print(insert_indicator_cmd.__doc__, file=sys.stderr)
            sys.exit(1)

        if options['ind_type'] is None:
            print('Missing option -t <indicator_type>', file=sys.stderr)
            print(insert_indicator_cmd.__doc__, file=sys.stderr)
            sys.exit(1)

        insert_indicator_cmd(options)

    # INSERT INDICATOR TO ALL USERS
    elif argv[0] == 'insert_indicator_to_all':

        if options['ind_type'] is None:
            print('Option -t <indicator_type> required', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        insert_indicator_to_all_cmd(options)

    # UPDATE FROM CSV FILE
    elif argv[0] == 'update_from_csv_file':

        if options['file_name'] is None:
            print('Command needs -f <filename>.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if options['column_names'] is []:
            print('Option -n <column_name> required', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if options['key_name'] is None:
            print('Remove Indicator needs option -k <keyname>', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if options['ind_type'] is None:
            print('Option -t <indicator_type> required', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        # If no -m option is given, use the -k by default.
        if options['compare_attr'] is None:
            options['compare_attr'] = options['key_name']

        update_from_csv_file_cmd(options)

    # DUMP CSV FILE
    elif argv[0] == 'dump_csv_file':

        if options['file_name'] is None:
            print('Command needs -f <filename>.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        dump_csv_file_cmd(options)

    elif argv[0] == 'indicator_to_personal_record':

        indicator_to_personal_record(options)

    else:
        print(argv[0], 'is an incorrect command.', file=sys.stderr)
    print(run.__doc__)
