#
# Copyright (C) 2015 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import sys

from django.db import connection

# Global variables
db_cursor = None  # Cursor obtained from the connection


def open_connection():
    """
    :return: Nothing

    Gets the db_cursor from the django db connection and stores it in  a
    global variable for this module.
    """

    global db_cursor

    # If the connection is open, return
    if db_cursor is not None:
        return

    db_cursor = connection.cursor()


def column_exists_in_table(table_name, column_name):
    query = """select count(*) from information_schema.columns 
               where table_name = %s and column_name = %s"""

    db_cursor.execute(query, (table_name, column_name))
    return db_cursor.fetchall()[0][0] != 0


def csv_dump_table(table_name, columns, file_out, filter_string):
    """

    :param table_name: Name of the table to dump
    :param columns: Subset of columns to consider
    :param file_out: File to dump the data
    :param filter_string to at at the end of the query if needed
    :return: Nothing. The file output is created

    """

    # Get the connection open
    open_connection()

    # DUMP the table directly from the DB for efficiency reasons.
    dump_csv_query = """COPY (select %s from %s %s)
                        TO STDOUT WITH CSV HEADER"""

    query = dump_csv_query % (','.join(columns), table_name, filter_string)

    print('Executing CSV DUMP', query)

    with open(file_out, 'w') as f:
        db_cursor.copy_expert(query, f)
