#!/bin/bash
#
# Script to execute the process function
#
# Author: Abelardo Pardo <abelardo.pardo@sydney.edu.au>
#
# export DJANGO_SETTINGS_MODULE=data2u.settings
# python \
#   $HOME/django/accumulate/execute_filter.py \
#   -f update_event_counters \
#   -f calculate_week_scores \
#   -f create_comments \
#   ELEC1601 \
#   >>$HOME/Logs/process.log \
#   2>&1

$HOME/django/manage.py \
  runscript \
  -v 1 \
  --traceback \
  execute_filter \
  --script-args="\
  -f update_event_counters \
  -f calculate_week_scores \
  -f create_comments \
  ELEC1601" \
  >>$HOME/Logs/process.log \
  2>&1

