#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from user_mgmt.models import D2User


class D2UserCreationForm(UserCreationForm):
    """
    A form that creates a user, with no privileges, from the given email and
    password. The purpose of this form is to remove the username from the fields
    that are considered when creating a user.
    """

    def __init__(self, *args, **kargs):
        super(D2UserCreationForm, self).__init__(*args, **kargs)
        del self.fields['username']

    class Meta:
        model = D2User
        fields = ("email",)
        exclude = []


class D2UserChangeForm(UserChangeForm):
    """
    A form for updating users. Includes all the fields on the user, but replaces
    the password field with admin's password hash display field.
    """

    def __init__(self, *args, **kargs):
        super(D2UserChangeForm, self).__init__(*args, **kargs)
        del self.fields['username']

    class Meta:
        model = D2User
        exclude = []
